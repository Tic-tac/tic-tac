package ru.edison.tictactoe.db.schema;

/**
 * @created RZaytseff 
 * @author Роман Витальевич
 * @Date 01.07.2015 16:11:36
 * @comment
 * @version 1.0
 */
public class State {

	private int id;
	private int state;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}

}