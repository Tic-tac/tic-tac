package ru.edison.tictactoe.db.schema;

/**
 * @created RZaytseff
 * @author Роман Витальевич 
 * @Date 01.07.2015
 * @comment
 * @version 1.0
 * @created 01-jul-2015 16:35:19
 */
public class Status {

	private int id;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}