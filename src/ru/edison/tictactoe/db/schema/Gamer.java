package ru.edison.tictactoe.db.schema;

/**
 * @created RZaytseff @author Роман Витальевич
 * @Date 01.07.2015 16:11:36
 * @comment
 * @version 1.0
 */
public class Gamer {

	private int id;
	private String nic;
	private String name;
	private String login;
	private String password;
	private String state;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNic() {
		return nic;
	}
	public void setNic(String nic) {
		this.nic = nic;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}