package ru.edison.tictactoe.db.dao;

import java.io.*;
import java.util.*;

import com.google.gson.Gson;

import ru.edison.tictactoe.server.message.GameMessageBean;
import ru.edison.tictactoe.server.message.GameStatusType;

public class GameDao {

	private final String GAME_FILE_FOLDER_NAME = "db/";
	private final String GAME_FILE_EXT = ".txt";
	
	private final char tab = 9;
	
	// для предотващения утечки памяти
	private File f;
	
	public int getScore(String playerX, String playerO) {
		String fileName = GAME_FILE_FOLDER_NAME + playerX + "-" + playerO + GAME_FILE_EXT;
    	RandomAccessFile raf = null;
    	try {
        	raf = new RandomAccessFile(fileName, "r");
        	return raf.readChar();
    	} catch (IOException e) {
       		System.err.println("Can't read score for players: "  + playerX + "-" + playerO);
    		return 0;
    	} finally {
    		if(raf != null) {
    			try {
    				raf.close();
    			} catch (Exception e) {
    	    		System.err.println("Don't read file name: " + fileName );
    				e.printStackTrace();
    			}
    		}
    		raf = null;
    	}
	}

	private synchronized RandomAccessFile openOrCreate(String fileName, String mode) throws IOException {
		f = new File(fileName);
	    if(!f.exists() || !f.canRead() || !f.canWrite()) {
	        
			  if(!f.createNewFile()) {
				  throw new IOException("Error reding Gamer List & attempt create new list was fault!");
			  } else {
				  if(!f.exists() || !f.canRead() || !f.canWrite()) {
						  throw new IOException("Error reding Gamer List & attempt create new list was fault!");
			      }
			  }
	    }
	    // для предотващения утечки памяти
	    f = null;
	    
		return new RandomAccessFile(fileName, mode);
	}
	
	public String getStatus(String playerX, String playerO) {
		String status = GameStatusType.GAME_UNDEFINED.value();
		String fileName = GAME_FILE_FOLDER_NAME + playerX + "-" + playerO + GAME_FILE_EXT;
    	RandomAccessFile raf = null;
    	try {
        	raf =  new RandomAccessFile(fileName, "r");
        	raf.readChar(); 
        	return status = GameStatusType.value(raf.readChar());
    	} catch (IOException e) {
    		System.err.println("Can't read score for players: "  + playerX + "-" + playerO);
        	return status;
    	} finally {
    		if(raf != null) {
    			try {
    				raf.close();
    			} catch (Exception e) {
                		System.err.println("Don't read file name: " + fileName );
 //       				e.printStackTrace();
    			}
    		}
    		// для предотващения утечки памяти
    		raf = null;
    	}
	}
	
	// RWS мода поддерживает синхронизацию записи
	public void  setScore(String playerX, String playerO, int score) {
		String fileName = GAME_FILE_FOLDER_NAME + playerX + "-" + playerO + GAME_FILE_EXT;
      	RandomAccessFile raf = null;
      	try {
        	raf = openOrCreate(fileName, "rws");
        	raf.writeChar(score);
      	} catch (Exception e) {
      		System.err.println("Can't set score for game with players: " + playerX + "->" + playerO );

      	} finally {
      		if(raf != null) {
      			try {
      				raf.close();
      			} catch (Exception e) {
            		System.err.println("Can't write to file name: " + fileName );
      				e.printStackTrace();
      			}
      		}
      		raf = null;
      	}
		return;
	}

	// RWS мода поддерживает синхронизацию записи
	public void  setStatus(String playerX, String playerO, String status) {
		String fileName = GAME_FILE_FOLDER_NAME + playerX + "-" + playerO + GAME_FILE_EXT;
      	RandomAccessFile raf = null;
      	try {
        	raf = openOrCreate(fileName, "rws");
        	try {
        		raf.readChar();
        	} catch (Exception e)	{
        		raf.writeChar(0);
        	}
        	raf.writeChar(GameStatusType.code(status));
      	} catch (Exception e) {
      		System.err.println("Don't set STATUS for game with players: " + playerX + "->" + playerO );
        	
      	} finally {
      		if(raf != null) {
      			try {
      				raf.close();
      			} catch (Exception e) {
            		System.err.println("Can't write to file name: " + fileName );
      				e.printStackTrace();
      			}
      		}
			raf = null;
      	}
		return;
	}

	public int incrementScore(String playerX, String playerO) {
		try {
			int score = getScore(playerX, playerO) + 1;
			setScore(playerX, playerO, score);
			return score;
		} catch (Exception e) {
      		System.err.println("Can't increment SCORE for game with players: " + playerX + "->" + playerO );
			return 0;
		}
	}
	
	public List<GameMessageBean> getGameStatusList(String player) {
		List<GameMessageBean> list = new LinkedList<GameMessageBean>();
		try {
			for(String gamer : GamerDao.getGamerList()) {
				String gamerNic = gamer.split(String.valueOf(tab))[0];
				if(!player.equalsIgnoreCase(gamerNic)) {
					GameMessageBean bean = new GameMessageBean();
					bean.setGamerX(player);
					bean.setGamerO(gamerNic);
					bean.setScore("" + getScore(player, gamerNic) + ":" +  + getScore(gamerNic, player));
					bean.setStatus(getStatus(player, gamerNic));
					list.add(bean);
				}
			}		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public String getGameStatusListJson(String player) {
		Gson gson = new Gson();
		return gson.toJson(getGameStatusList(player));
	}

    public void main(String[] argv) throws IOException {
    	setScore("pioner", "looser", 9);
    	setStatus("pioner", "looser", GameStatusType.GAME_READY_TO_PLAY.value());
    	System.out.println(getScore("pioner", "looser") + "/" + getStatus("pioner", "looser") );
    	
    	setScore("pioner", "Mity", 19);
    	setStatus("pioner", "Mity", GameStatusType.GAME_INVITE.value());
    	System.out.println(getScore("pioner", "Mity") + "/" + getStatus("pioner", "Mity") );

    	setScore("pioner", "товарищ", 29);
    	setStatus("pioner", "товарищ", GameStatusType.GAME_REFUSE.value());
    	System.out.println(getScore("looser", "товарищ") + "/" + getStatus("looser", "товарищ") );
    	
    	setScore("pioner", "чувак", 39);
    	setStatus("pioner", "чувак", GameStatusType.GAME_READY_TO_PLAY.value());
    	System.out.println(getScore("pioner", "чувак") + "/" + getStatus("pioner", "чувак") );

    	incrementScore("pioner", "Uliss"); setStatus("pioner", "Uliss", GameStatusType.GAME_IN_PLAY.value());
    	
    	System.out.println(getScore("pioner", "looser") + "/" + getStatus("pioner", "looser") );
    	System.out.println(getScore("pioner", "Mity") + "/" + getStatus("pioner", "Mity") );
    	System.out.println(getScore("pioner", "товарищ") + "/" + getStatus("pioner", "товарищ") );
    	System.out.println(getScore("pioner", "чувак") + "/" + getStatus("pioner", "чувак") );
    	System.out.println(getScore("pioner", "Uliss") + "/" + getStatus("pioner", "Uliss") );
   	    	
    	System.out.println(getGameStatusListJson("pioner"));
    }
}
