package ru.edison.tictactoe.game;

import org.jboss.netty.channel.Channel;

import ru.edison.tictactoe.game.Game.PlayerLetter;

/**
 * Represents a player for a game of Tic Tac Toe.
 * @author Kevin Webber, see http://kevinwebber.ca/multiplayer-tic-tac-toe-in-java-using-the-websocket-api-netty-nio-and-jquery/
 * @Adapted by RZaytsev - Роман Витальевич
 */
public class Player {
	
	// The player's websocket channel. Used for communications.
	private Channel channel; 
	
	// The player's currently assigned letter.
	private PlayerLetter letter;
	
	private String nicName;
	
	public Player(Channel c, String nic) {
		channel = c;
		nicName = nic;
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	public void setLetter(PlayerLetter l) {
		letter = l;
	}
	
	public PlayerLetter getLetter() {
		return letter;
	}
	
	public String getNic() {
		return nicName;
	}
	
}
