package ru.edison.tictactoe.server.message;

/**
 * Base class of message beans
 * @author RZaytsev
 */
public class GameMessageBean extends  MessageBean {
	
	private String gamerX; /* gamerX means a first player, not X  only */
	private String gamerO; /* gamerO means a first player, not O  only */
	private String state;  
	private String score; /* score have format:  "scoreX:scoreO" or "--:-- if absent or "0:0" if game is begin right now*/
	private String status;
	
	public GameMessageBean() {
		super();
		this.type = MessageType.GAME_STATE;
	}
	
	public GameMessageBean(String gamerX, String gamerO) {
		this();
		this.gamerX = gamerX;
		this.gamerO = gamerO;
	}
	
	public GameMessageBean(String gamerX, String gamerO, String state) {
		this(gamerX, gamerO);
		this.state = state;
	}
	
	public GameMessageBean(String gamerX, String gamerO, String state, String score, String status) {
		this(gamerX, gamerO, state);
		this.score = score;
		this.state = state;
	}
	
	public GameMessageBean(String type, String gamerX, String gamerO, String state, String score, String status) {
		this(gamerX, gamerO, state, score, status);
		this.type = type;
	}
	
	public String getGamerX() {
		return gamerX;
	}
	public void setGamerX(String gamerX) {
		this.gamerX = gamerX;
	}
	public String getGamerO() {
		return gamerO;
	}
	public void setGamerO(String gamerO) {
		this.gamerO = gamerO;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
