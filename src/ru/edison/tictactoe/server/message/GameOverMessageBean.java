package ru.edison.tictactoe.server.message;

/**
 * Game over message
 * @author Kevin Webber, see http://kevinwebber.ca/multiplayer-tic-tac-toe-in-java-using-the-websocket-api-netty-nio-and-jquery/
 * @Adapted by RZaytsev - Роман Витальевич
 */
public class GameOverMessageBean extends MessageBean {
	public enum Result {
		YOU_WIN, TIED
	}
	
	private Result result;
	
	public GameOverMessageBean(Result r) {
		type = "game_over";
		result = r;
	}
	
	public String getType() {
		return "game_over";
	}
	
	public Result getResult() {
		return result;
	}
}
