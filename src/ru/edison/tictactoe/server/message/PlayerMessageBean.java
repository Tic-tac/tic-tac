package ru.edison.tictactoe.server.message;

/**
 * Represents incoming message from the user
 * @author Роман Витальевич
 */
public class PlayerMessageBean extends MessageBean {
	
	private int gameId;
	private String player;
	private String gridId;
	private String nic;
	
	public String getType() {
		return "game";
	}
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getGridId() {
		return gridId;
	}
	public void setGridId(String gridId) {
		this.gridId = gridId;
	}
	public int getGridIdAsInt() {
		return Integer.valueOf(gridId.substring(gridId.length() - 1));
	}
	public String getNic() {
		return nic;
	}
	public void setNic(String nic) {
		this.nic = nic;
	}
}
