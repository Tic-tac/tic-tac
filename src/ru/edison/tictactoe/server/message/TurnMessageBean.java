package ru.edison.tictactoe.server.message;

/**
 * Represents a message of Tic Tac Toe turn
 * @author Kevin Webber, see http://kevinwebber.ca/multiplayer-tic-tac-toe-in-java-using-the-websocket-api-netty-nio-and-jquery/
 * @Adapted by RZaytsev - Роман Витальевич
 */
public class TurnMessageBean extends MessageBean {
	
	public enum Turn {
		WAITING, YOUR_TURN
	}
	
	private Turn turn;

	public TurnMessageBean(Turn t) {
		type = "turn";
		turn = t;
	}

	public String getType() {
		return type;
	}
	
	public Turn getTurn() {
		return turn;
	}	
	
}
