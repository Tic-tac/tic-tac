package ru.edison.tictactoe.server.message;

public interface MessageType {
	
	// протокольное HTTP приветствие и создание канала WebSocket 
	String HANDSHAKE = "handshake";
	
	String LOGIN = "login";
	String REGISTRATION = "registration";
	
	// запрос всех не он-лайн пользователей
	String GAMER_LIST = "gamerList";
	// запрос текущим игроком результатов игр для всех он-лайн пользователей 
	String GAMER_PRESENCE_STATE_LIST = "presenceList";
	
	
	// изменения в процессе игры
	String GAME = "game";
	String SCORE = "score";
	String SCORE_INCREMENT = "scoreAutoIncremen";
	String OUTGOING = "response";
	String TURN = "turn";
	String GAME_OVER = "game_over";
	String GAMESET_OVER = "gameSetOver";
	String GAME_OVER_WIN = "YOU_WIN";
	String GAME_OVER_LOSE = "TIED";

	// изменение состояний и статусов:
	String GAMER_PRESENCE_STATE = "gamerPresenceState";
	String GAMER_STATE = "gamerState";
	String GAME_INVITE = "invite";
	String GAME_INVITED = "invited";
	String GAME_INVITATION = "invitation";
	String GAME_SCORE = "gameScore";
	String GAME_STATUS = "gameStatus";
	String GAME_STATE = "gameState";
	String GAME_STATE_LIST = "gameStateList";
	String GAME_CONFIRM_INVITE = "confirm";
	String GAME_CONFIRM_INVITED = "confirmed";
	String GAME_REFUSE_INVITE = "refuse";
	
	// запрос состояний и статусов:
	String GET_GAMER_PESENCE_STATE = "getPresenceState";
	String GET_GAMER_STATE = "getState";
	String GET_GAME_SCORE =  "getScore";
	String GET_GAME_STATUS = "getStatus";

	String GET_FILE = "file";

	String GET_DICTIONARY = "dictionary";

	String GET_DICTIONARY_LIST = "stringList";


}
