var expandPanel = (function($) {
	var configMap = {
			extended_height: 333,
			extended_title: "",
			retracted_height: 16,
			retracted_title: "",
			template_html: '<div class="spa-slider"></div>'
			};
	var $chatSlider, toggleSlider, onClickSlider, initModule;
	toggleSlider = function() {
		var slider_height = $chatSlider.height();
		if(slider_height === configMap.retracted_height) {
			$chatSlider
				.animate({height: configMap.extended_height})
				.attr('title', configMap.extended_title);
			return true;
		}
		if(slider_height === configMap.extended_height) {
			$chatSlider
				.animate({height: configMap.retracted_height})
				.attr('title', configMap.retracted_title);
			return true;
		}
		return false;
	};
	onClickSlider = function(event) {
		toggleSlider();
		return false;
	};
	initModule = function ($container) {
		$chatSlider = $container.find('.spa-slider'); 
		$chatSlider.attr('title', configMap.extended_title);
		
		$chatHeaderSlider = $container.find('#gamerScoreListHeadrDiv');
		$chatHeaderSlider.click(onClickSlider);  
//		toggleSlider();
		return true;
	}; 
	return {initModule: initModule, toggleSlider: toggleSlider}; 
})(jQuery);

function initSlidePanel($container) {
	expandPanel.initModule($container);
}

function toggleSlider() {
	expandPanel.toggleSlider();
}

